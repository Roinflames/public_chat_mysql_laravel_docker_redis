<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Public chat</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet" type="text/css">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 12px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
        </style>
    </head>
    <body>
        <div class="flex-center position-ref full-height">
            <div class="content">
                <div class="title m-b-md">
                    Public Chat
                </div>               

                <textarea rows="15" cols="150">                    
                    @foreach($comentarios as $comentario)
                        {{ $comentario }}                        
                    @endforeach                   
                </textarea><br>
                <a href="index"> 
                    Ver todos los comentarios.
                </a>

                <form action="comentario" method="POST">
                {{ $return }}
                <br><br>
                    @csrf                
                    id <input type="text" name="id">
                    nombre <input type="text" name="nombre">
                    comentario <input type="text" name="comentario">                         
                    <input type="submit" value="Enviar">
                </form>    
                <br><br>
                <form action="archivo" method="POST" enctype="multipart/form-data">                                
                    @csrf        
                    id <input type="text" name="id" required>                            
                    archivo <input type='file' 
                                   name='adjuntos' 
                                   id='adjuntos' 
                                   accept=".mp4,.flv,.xvid,.divx,.jpg,.png"                                    
                                   multiple>               
                    <input type="submit" value="Enviar">
                </form>           
            </div>
        </div>
    </body>
</html>
