<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//ver/Guardar/enviar comentario
Route::get('/', ['as' => 'home', 'uses' => 'ComentarioController@verComentario']);
Route::post('comentario', ['as' => 'comentario', 'uses' => 'ComentarioController@agregarComentario']);
//enviar archivo
Route::post('archivo', ['as' => 'archivo', 'uses' => 'ComentarioController@agregarArchivo']);
//leer comentarios
Route::get('index', ['as' => 'index', 'uses' => 'ComentarioController@index']);