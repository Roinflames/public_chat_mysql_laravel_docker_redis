<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Redis;

use Illuminate\Http\File;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Request;
use DB;
use App\Comentario;

class ComentarioController extends Controller
{
    public function index(){        
        $redis = Redis::connection();
        $allKeys = count($redis->keys('*:comentario'));        
        
        for ($i = 0; $i <= $allKeys; $i++) {
            $comentarios[$i] = Redis::get('comentario:'. $i .':comentario');
        }              
        return view('welcome', ['comentarios' => $comentarios, 'return' => '']);        
    }

    public function verComentario(){ 
        $redis = Redis::connection();                
        $allKeys = count($redis->keys('*:comentario'));        
        for ($i = 0; $i <= $allKeys; $i++) {
            $nombres[$i] = Redis::get('comentario:'. $i .':nombre');
            $comentarios[$i] = Redis::get('comentario:'. $i .':comentario');
        }              
        return view('welcome', ['comentarios' => $comentarios, 'nombres' => $nombres, 'return' => '']);        
    }

    public function agregarComentario(Request $request){
        $return = [];
    	$data = $request->all();
        
        if (!empty($data)) {
            $comentario = new Comentario;

            $comentario->id = $data['id'];
            $comentario->nombre = $data['nombre'];
            $comentario->comentario = $data['comentario'];
            $comentario->adjuntos = '';

            if($comentario->save()){  
                $redis = Redis::connection(); 

                $uri_id = "comentario:". $data['id'] .":id"; 
                $uri_nombre = "comentario:". $data['id'] .":nombre"; 
                $uri_comentario = "comentario:". $data['id'] .":comentario"; 
                
                Redis::set($uri_id, $comentario['id']);
                Redis::set($uri_nombre, $comentario['nombre']);
                Redis::set($uri_comentario, $comentario['comentario']);

                $allKeys = count($redis->keys('*:comentario'));        
                for ($i = 0; $i <= $allKeys; $i++) {
                    $comentarios[$i] = Redis::get('comentario:'. $i .':comentario');
                }     

                $return ['status'] = true;
                $return ['msg'] = 'Mensaje recibido con éxito...';                
            }else{
                $return ['status'] = false;
                $return ['msg'] = 'Error al recibir el mensaje...';
            }
            return view('welcome', ['comentarios' => $comentarios, 'return' => $return['msg']]);
        } else {
            return 'no se han encontrado datos...';
        }        
    }

    public function agregarArchivo(Request $request){
        $return = [];
    	$data = $request->all();
        
        if (!empty($data)) {
            $comentario = new Comentario;   
            
            $comentario->id = $data['id'];
            $comentario->nombre = '';
            $comentario->comentario = '';
            $comentario->adjuntos = $data['adjuntos'];
            
            if($comentario->save()){          
                $redis = Redis::connection(); 

                $uri_id = "comentario:". $data['id'] .":id";  
                $uri_adjuntos = Storage::disk('local')->put('public', $comentario->adjuntos);

                Redis::set($uri_id, $comentario['id']);
                Redis::set($uri_adjuntos, $comentario['adjuntos']);     
                
                $allKeys = count($redis->keys('*:comentario'));    
                
                for ($i = 0; $i <= $allKeys; $i++) {
                    $comentarios[$i] = Redis::get('comentario:'. $i .':comentario');
                }   
            }            
            
            return view('welcome', ['comentarios' => $comentarios, 'return' => 'Archivo guardado con éxito']);
        }
    }
}
