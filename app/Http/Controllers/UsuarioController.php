<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class UsuarioController extends Controller
{
    public function index(){
        $usuarios = DB::table('usuario')
			->select()			
            ->get();
        return $usuarios;
    }   
}
